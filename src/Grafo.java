import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Grafo {
//-------------------------ESTRUTURA:
	private class Nodo {

		public String nome;
		public int marca;
		public HashMap<String, Nodo> irmaos;
		public Nodo p;
		public int d;


		public Nodo(String nome) {
			this.nome = nome;
			this.marca = 0;
			irmaos = new HashMap<String, Nodo>();
			p = null;
			d = 0;
		}

		public void adicionarIrmao(String conta, Nodo n) {
			this.irmaos.put(conta, n);
		}

	}

	private ArrayList<Nodo> nodos;
	private int quantNodos;
	private int quantArestas;


	public Grafo () {
		this.nodos = new ArrayList<Nodo>();
		this.quantNodos = 0;
		this.quantArestas= 0;
	}

	public void adicionaConta(String conta, String a, String b) {
		if(!existeCorrentista(a)) {
			nodos.add(new Nodo(a));
			quantNodos++;
		}
		if(!existeCorrentista(b)) {
			nodos.add(new Nodo(b));
			quantNodos++;
		}
		Nodo nodoA = getNodoPorNome(a);
		Nodo nodoB = getNodoPorNome(b);
		nodoA.adicionarIrmao(conta, nodoB);
		nodoB.adicionarIrmao(conta, nodoA);
		quantArestas++;
	}

	public boolean existeCorrentista(String nome) {
		for(Nodo n: nodos) {
			if(n.nome.equals(nome)) {
				return true;
			}
		}
		return false;
	}

	public Nodo getNodoPorNome(String nome) {
		for(Nodo n: nodos) {
			if(n.nome.equals(nome)) {
				return n;
			}
		}
		return null;
	}


//-------------------------------------TRABALHO:
	public void menorCaminho (String origem, String destino) {
		Nodo inicial = getNodoPorNome(origem);
		Nodo terminal = getNodoPorNome(destino);
		String resposta = "";

		dijkstra(inicial);

		ArrayList<String> caminho = new ArrayList<>();

		Nodo aux = terminal;
		String conta = "";
		while(aux.nome != inicial.nome) {
			for(String key: aux.irmaos.keySet()) {
				if(aux.irmaos.get(key).nome.equals(aux.p.nome)) {
					conta = key;
				}
			}
			caminho.add(conta+" ("+aux.p.nome +" "+aux.nome + ")");

			aux = aux.p;
		}
		Collections.reverse(caminho);
		caminho.forEach(n -> {System.out.println(n);});
	}



	public void dijkstra(Nodo inicial) {
		nodos.forEach(w -> {
			w.d = Integer.MAX_VALUE;
			w.p = null;
			w.marca=0;
		});
		inicial.d = 0;

		Nodo u;
		while( (u = min()) != null) {
			u.marca=1;
			for(Nodo w: u.irmaos.values()) {
				if( (u.d + 1) < w.d) {
					w.d = u.d + 1;
					w.p = u;
				}
			}
		}
	}


	public Nodo min() {
		int menor = Integer.MAX_VALUE;
		Nodo resp = null;
		for(Nodo n: nodos) {
			if(n.marca==0 && n.d < menor){
				menor = n.d;
				resp = n;
			}
		};
		return resp;
	}






//---------------------------------------------EXTRAS:
	public void caminhoProfundidade () {
		Nodo inicio = getNodoPorNome("Luis");
		System.out.println(inicio.nome);
		nodos.forEach(n -> {n.marca = 0;});
		inicio.marca = 1;
		ArrayList<Nodo> fila = transformaEmArrayList(inicio.irmaos);
		//fila.forEach(n->{System.out.println("|"+n.nome+"|");});
		while(!fila.isEmpty()) {
			//fila.forEach(n->{System.out.print("|"+n.nome+"|");});
			Nodo primeiro = fila.remove(fila.size()-1);
			primeiro.marca=1;
			System.out.println(primeiro.nome);
			for(Nodo n: primeiro.irmaos.values()) {
				if(n.marca == 0){
					fila.add(n);
					n.marca=1;
				}
			}

		}
	}

	public ArrayList<Nodo> transformaEmArrayList(HashMap<String, Nodo> vizinhos) {
		ArrayList<Nodo> resp = new ArrayList();
		for(Nodo v: vizinhos.values()) {
			resp.add(v);
		}
		return resp;
	}


	public int quantosCaminhos (Nodo a, Nodo b) {
		if(a.nome.equals(b.nome)){return 1;}
		a.marca=1;
		int acc=0;
		for(Nodo n: a.irmaos.values()) {
			if(n.marca==0){
				acc= acc+quantosCaminhos(n, b);

			}
		}
		a.marca=0;
		return acc;
	}
}
