import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class App {

	public static Grafo contas = new Grafo();
	public static Scanner in = new Scanner (System.in);

	public static void main(String [] args) {
		menu();
	}

	public static void menu() {
		System.out.println("Informe o nome do arquivo:");
		String arq = in.nextLine();
		carregaCasos(arq);
	}

	public static void carregaCasos(String arq) {
		Path path = Paths.get(arq);
		try(Scanner reader = new Scanner(Files.newBufferedReader(path, Charset.forName("utf-8")))) {
			int numeroDeContas = Integer.parseInt(reader.nextLine());
			//System.out.println(numeroDeContas);

			String numeroConta, correntista1, correntista2;
			String linha[] = new String[3];
			for(int i=1; i <= numeroDeContas; i++){
				linha = reader.nextLine().split(" ");
				numeroConta = linha[0];
				correntista1 = linha[1];
				correntista2 = linha[2];
				//System.out.println(numeroConta + " " + correntista1 + " " + correntista2);
				contas.adicionaConta(numeroConta, correntista1, correntista2);

			}
			linha = reader.nextLine().split(" ");
			String origem = linha[0];
			String destino = linha[1];
//			System.out.println(origem+" " + destino);
//			System.out.println("-----------------------");
			contas.menorCaminho(origem, destino);
		}
		catch(IOException e) {
			System.out.println("Arquivo n�o encontrado!");
		}
	}
}
